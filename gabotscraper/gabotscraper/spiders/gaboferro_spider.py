import scrapy
from gabotscraper.items import GabotscraperItem


class GaboSpider(scrapy.Spider):
    name = "gaboferro"
    urlsgabo = [
        'https://gaboferro.com.ar/portfolio/canciones-que-un-hombre-no-deberia-cantar/',
        'https://gaboferro.com.ar/portfolio/todo-lo-solido-se-desvanece-en-el-aire/',
        'https://gaboferro.com.ar/portfolio/manana-no-debe-seguir-siendo-esto/',
        'https://gaboferro.com.ar/portfolio/amar-temer-partir/',
        'https://gaboferro.com.ar/portfolio/boca-arriba/',
        'https://gaboferro.com.ar/portfolio/el-hambre-y-las-ganas-de-comer/',
        'https://gaboferro.com.ar/portfolio/la-aguja-tras-la-mascara/',
        'https://gaboferro.com.ar/portfolio/la-primera-noche-del-fantasma/',
        'https://gaboferro.com.ar/portfolio/el-veneno-de-los-milagros/',
        'https://gaboferro.com.ar/portfolio/el-lapsus-del-jinete-ciego/',
        'https://gaboferro.com.ar/portfolio/el-agua-del-espejo/',
    ]

    def start_requests(self):

        # Lista de urls para iterar páginas siguientes a la primera
        urls = self.urlsgabo

        # Selector para iterar sobre todas las páginas o solo la primera
        full_iteration = True

        #Scrapeo completo
        if full_iteration == True:
            pass

        # Scrapeo de primera página únicamente
        else:
            urls = ['https://gaboferro.com.ar/portfolio/canciones-que-un-hombre-no-deberia-cantar/']

        for url in urls:
            print('|||||||||||| ' + str(url) + ' ||||||||||||')
            yield scrapy.Request(url=url, callback=self.parse)

    # Función de scrapper
    def parse(self, response):
        xpath = "//article[@class='entry']"

        # Instancia del Item
        item = GabotscraperItem()

        # Canciones
        songs = response.xpath('//article').get().split('<h4>')
        for song in songs:
            try:
                item['title'] = song.split('</h4>')[0]
                if '<div ' in song:
                    item['lyrics'] = song.split('</h4>')[1].replace('<p>', '').replace('</p>', '').replace('<br>', ' ').replace('\n', '/').split('<div ')[0]
                else:
                    item['lyrics'] = item['lyrics'] = song.split('</h4>')[1].replace('<p>', '').replace('<br>', ' ').replace('</p>', '').replace('\n', '/')
                yield item
            except:
                continue
